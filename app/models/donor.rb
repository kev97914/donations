class Donor < ActiveRecord::Base
  has_many :donations
  validates_presence_of :name, :email
  validates_uniqueness_of :name
end

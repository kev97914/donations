class Donation < ActiveRecord::Base
  belongs_to :donor
  validates_presence_of :amount, :date
  validates_numericality_of :amount, :greater_than_or_equal_to => 0.01

end
